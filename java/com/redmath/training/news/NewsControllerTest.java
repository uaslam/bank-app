package com.redmath.training.news;

import java.nio.charset.Charset;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class NewsControllerTest {

    public static final MediaType JSON = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetNewsByIdSuccess() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/news/123"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.comparesEqualTo(123)));
    }

    @Test
    public void testGetNewsByIdNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/news/456"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void testCreateNewsSuccess() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/news")
                .with(SecurityMockMvcRequestPostProcessors.user("editor").password("editor")
                        .authorities(AuthorityUtils.commaSeparatedStringToAuthorityList("EDITOR")))
                .with(SecurityMockMvcRequestPostProcessors.csrf())
                .contentType(JSON)
                .content("{\"id\": 456, \"title\": \"News 456\"}"))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }
}
