package com.Training.BankApp.Config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class webConfig extends WebMvcConfigurerAdapter {
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
    	
    	
    	registry.addViewController("/localhost:3000").setViewName("/login");
    	registry.addRedirectViewController("/localhost:8080/login", "https://localhost:3000");
        
        
           }
}
