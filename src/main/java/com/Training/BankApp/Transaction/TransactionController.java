package com.Training.BankApp.Transaction;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransactionController {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private TransactionService transactionService;

	public TransactionController(TransactionService transactionService) {
		super();
		this.transactionService = transactionService;
	}

	@GetMapping("/api/transaction/get")
	public ResponseEntity<List<Transaction>> getAccounts() {
		return ResponseEntity.ok(transactionService.findAll());
	}

	@GetMapping("/api/transaction/get/{id}")
	public ResponseEntity<Transaction> getTransactionById(@PathVariable("id") Long id) {

		Transaction transaction = transactionService.findById(id);
		if (transaction != null) {
			return ResponseEntity.ok(transaction);
		}
		return ResponseEntity.notFound().build();
	}

	/*@PostMapping("/api/transaction/add/{id}")
	public ResponseEntity<Void> AddTransactionRecord(@PathVariable("id") Long id, @RequestBody Transaction transaction) {

		if (transactionService.create(id,transaction)) {
			logger.info("Transaction with  ID: " + transaction.getId() + " successfully created.");
			return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
					.buildAndExpand(transaction.getId()).toUri()).build();
		}

		return ResponseEntity.badRequest().build();
	}*/

	/*@PostMapping("/api/account/add/{id}")
	public ResponseEntity<Void> UpdateAccount(@PathVariable("id") Long id, @RequestBody Transaction transaction) {

		Transaction existing = transactionService.findById(id);
		if (existing == null) {
			logger.warn("Customer with  ID: " + id + " does not exists for updation.");
			return ResponseEntity.badRequest().build();
		}

		transaction.setId(id);
		transactionService.update(transaction);
		logger.info("Customer with  ID: " + id + " successfully updated.");
		return ResponseEntity.ok().build();

	}
*/
	@DeleteMapping("/api/transaction/delete/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
		Transaction existing = transactionService.findById(id);
		if (existing != null) {
			transactionService.delete(id);
			logger.info("Transaction with  ID: " + id + " successfully deleted.");
			return ResponseEntity.ok().build();
		}
		logger.warn("Transaction with  ID: " + id + " does not exists for deletion");
		return ResponseEntity.badRequest().build();
	}
}
