package com.Training.BankApp.Transaction;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class TransactionService {

	private TransactionRepository transactionRepository;

	public TransactionService(TransactionRepository transactionRepository) {
		super();
		this.transactionRepository = transactionRepository;
	}

	public List<Transaction> findAll() {
		// TODO Auto-generated method stub
		return transactionRepository.findAll();
	}

	public Transaction findById(Long id) {
		// TODO Auto-generated method stub
		return transactionRepository.findOne(id);
	}

	public boolean create(Transaction transaction) {
		// TODO Auto-generated method stub
		return false;
	}

	public void update(Transaction transaction) {
		// TODO Auto-generated method stub
		
	}

	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}
	
	
}
