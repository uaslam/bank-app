package com.Training.BankApp.Transaction;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.Training.BankApp.Account.Account;



@Entity(name = "transactions")
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private Date date;
	private String description;
	private int amount;
	private boolean indicator;
	
	
	@ManyToOne
	private Account account;
	

	public Transaction(Date date, String description, int amount, boolean indicator) {
		super();
		this.date = date;
		this.description = description;
		this.amount = amount;
		this.indicator = indicator;
	}

	public Transaction() {
		super();
	}


	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isIndicator() {
		return indicator;
	}

	public void setIndicator(boolean indicator) {
		this.indicator = indicator;
	}

}
