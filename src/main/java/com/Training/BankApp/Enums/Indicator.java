package com.Training.BankApp.Enums;

public enum Indicator {
	Credit,
	Debit;
}
