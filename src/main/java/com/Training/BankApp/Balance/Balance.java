package com.Training.BankApp.Balance;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.Training.BankApp.Account.Account;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity(name = "balance_details")
public class Balance {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private Date date;
	private int Amount;
	private boolean indicator; // 0 means debit 1 means credit

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "balance")
	@JsonBackReference
	Account account;

	public Balance() {
		super();
	}

	public Balance(Date date, int amount, boolean indicator) {
		super();
		this.date = date;
		Amount = amount;
		this.indicator = indicator;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getAmount() {
		return Amount;
	}

	public void setAmount(int amount) {
		Amount = amount;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isIndicator() {
		return indicator;
	}

	public void setIndicator(boolean indicator) {
		this.indicator = indicator;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

}
