package com.Training.BankApp.Balance;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class BalanceController {
	private BalanceService balanceService;
	private final Logger logger = LoggerFactory.getLogger(getClass());

	public BalanceController(BalanceService balanceService) {
		super();
		this.balanceService = balanceService;
	}

	@GetMapping("/api/balance/get")
	public ResponseEntity<List<Balance>> getBalance() {
		return ResponseEntity.ok(balanceService.findAll());
	}

	@GetMapping("/api/balance/get/{id}")
	public ResponseEntity<Balance> getAccountbyId(@PathVariable("id") Long id) {

		Balance balance= balanceService.findById(id);
		if (balance != null) {
			return ResponseEntity.ok(balance);
		}
		return ResponseEntity.notFound().build();
	}

	@PostMapping("/api/balance/add")
	public ResponseEntity<Void> CreateBalance(@RequestBody Balance balance) {

		if (balanceService.create(balance)) {
			
			return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
					.buildAndExpand(balance.getId()).toUri()).build();
		}

		return ResponseEntity.badRequest().build();
	}

	@PostMapping("/api/balance/add/{id}")
	public ResponseEntity<Void> UpdateBalance(@PathVariable("id") Long id, @RequestBody Balance balance) {

		Balance existing = balanceService.findById(id);
		if (existing == null) {
			logger.warn("Balance with  ID: " + id + " does not exists for updation.");
			return ResponseEntity.badRequest().build();
		}

		balance.setId(id);
		balanceService.update(balance);
		logger.info("Balance with  ID: " + id + " successfully updated.");
		return ResponseEntity.ok().build();

	}

	@DeleteMapping("/api/balance/delete/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
		Balance existing = balanceService.findById(id);
		if (existing != null) {
			balanceService.delete(id);
			logger.info("Balance with  ID: " + id + " successfully deleted.");
			return ResponseEntity.ok().build();
		}
		logger.warn("Balance with  ID: " + id + " does not exists for deletion");
		return ResponseEntity.badRequest().build();
	}

}
