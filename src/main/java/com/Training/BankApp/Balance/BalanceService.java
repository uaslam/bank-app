package com.Training.BankApp.Balance;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class BalanceService {
	BalanceRepository balanceRepository;

	public BalanceService(BalanceRepository balanceRepository) {
		super();
		this.balanceRepository = balanceRepository;
	}

	public List<Balance> findAll() {
		// TODO Auto-generated method stub
		return balanceRepository.findAll();
	}

	public Balance findById(Long id) {
		// TODO Auto-generated method stub
		return balanceRepository.findOne(id);
	}

	public void update(Balance balance) {
		// TODO Auto-generated method stub
		balanceRepository.save(balance);
	}

	public void delete(Long id) {
		// TODO Auto-generated method stub
		balanceRepository.delete(id);
	}

	public boolean create(Balance balance) {
		
		
		
		
		return false;
	}

}
