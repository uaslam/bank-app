package com.Training.BankApp.Account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

	@Query("SELECT count(*) FROM  users u where u.email=?1 ")
	int findByEmail(String email);

}