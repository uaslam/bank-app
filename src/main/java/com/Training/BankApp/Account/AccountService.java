package com.Training.BankApp.Account;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.Training.BankApp.Balance.Balance;
import com.Training.BankApp.Transaction.Transaction;
import com.Training.BankApp.Util.CurrentDate;

@Service
public class AccountService// implements UserDetailsService {
{
	AccountRepository accountRespository;
	private final Logger logger = LoggerFactory.getLogger(getClass());

	public AccountService(AccountRepository repository) {
		super();
		this.accountRespository = repository;
	}

	public Account findById(Long id) {

		return accountRespository.findOne(id);

	}

	public List<Account> findAll() {
		return accountRespository.findAll();
	}

	public boolean create(Account account) {

		if (accountRespository.findByEmail(account.getUser().getEmail()) != 0) {
			logger.warn("Customer with  email: " + account.getUser().getEmail() + " already exists");
			return false;
		}

		Balance balance = new Balance();
		balance.setDate(CurrentDate.getDateTime());
		balance.setAccount(account);
		account.setBalance(balance);

		accountRespository.save(account);
		return true;

	}
	/*
	 * @Override public UserDetails loadUserByUsername(String email) throws
	 * UsernameNotFoundException { // TODO Auto-generated method stub
	 * 
	 * Account user = accountRespository.findByEmail(email); if (user == null) {
	 * throw new UsernameNotFoundException("Invalid user"); } return new
	 * org.springframework.security.core.userdetails.User(user.getEmail(),
	 * user.getPassword(),
	 * AuthorityUtils.commaSeparatedStringToAuthorityList(user.getRoles()));
	 * 
	 * }
	 */

	public void updateTransactions(Account account, Transaction transaction) {

		account.getTransaction().add(transaction);

		accountRespository.save(account);
		logger.warn("Transactions List updated.");
	}

	public boolean delete(Long id) {
		// TODO Auto-generated method stub
		accountRespository.delete(id);
		return true;
	}

	public void update(Account account) {
		accountRespository.save(account);
	}

}
