package com.Training.BankApp.Account;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.Training.BankApp.Transaction.Transaction;

@CrossOrigin(origins = "http://localhost:3000")

@RestController
public class AccountController {

	private AccountService accountService;
	private final Logger logger = LoggerFactory.getLogger(getClass());

	public AccountController(AccountService accountService) {
		super();
		this.accountService = accountService;
	}

	@GetMapping("/api/account/get")
	public ResponseEntity<List<Account>> getAccounts() {
		return ResponseEntity.ok(accountService.findAll());
	}

	@GetMapping("/api/account/get/{id}")
	public ResponseEntity<Account> getAccountbyId(@PathVariable("id") Long id) {

		Account account = accountService.findById(id);
		if (account != null) {
			return ResponseEntity.ok(account);
		}
		return ResponseEntity.notFound().build();
	}

	@PostMapping("/api/account/add")
	public ResponseEntity<Void> RegisterAccount(@RequestBody Account account) {

		if (accountService.create(account)) {
			logger.info("Customer with  ID: " + account.getUser().getId() + " successfully created.");
			return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
					.buildAndExpand(account.getId()).toUri()).build();
		}

		return ResponseEntity.badRequest().build();
	}

	@PostMapping("/api/account/add/{id}")
	public ResponseEntity<Void> UpdateAccount(@PathVariable("id") Long id, @RequestBody Account account) {

		Account existing = accountService.findById(id);
		if (existing == null) {
			logger.warn("Customer with  ID: " + id + " does not exists for updation.");
			return ResponseEntity.badRequest().build();
		}

		account.setId(id);
		accountService.update(account);
		logger.info("Customer with  ID: " + id + " successfully updated.");
		return ResponseEntity.ok().build();

	}
	@PostMapping("/api/account/transaction/add/{id}")
	public ResponseEntity<Void> AddTransactionToAccount(@PathVariable("id") Long id, @RequestBody Transaction transaction) {

		Account existing = accountService.findById(id);
		if (existing == null) {
			logger.warn("Customer with  ID: " + id + " does not exists for updation.");
			return ResponseEntity.badRequest().build();
		}
		if (transaction == null) {
			logger.warn("Transaction parameter passed is null.");
			return ResponseEntity.badRequest().build();
		}
		accountService.updateTransactions(existing,transaction);
		logger.info("Customer with  ID: " + id + " successfully updated.");
		return ResponseEntity.ok().build();

	}


	@DeleteMapping("/api/account/delete/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
		Account existing = accountService.findById(id);
		if (existing != null) {
			accountService.delete(id);
			logger.info("Customer with  ID: " + id + " successfully deleted.");
			return ResponseEntity.ok().build();
		}
		logger.warn("Customer with  ID: " + id + " does not exists for deletion");
		return ResponseEntity.badRequest().build();
	}

}
