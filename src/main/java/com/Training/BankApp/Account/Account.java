package com.Training.BankApp.Account;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.Training.BankApp.Balance.Balance;
import com.Training.BankApp.Transaction.Transaction;
import com.Training.BankApp.User.User;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity(name = "accounts")
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id")
	@JsonManagedReference
	private User user;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "balance_id")
	@JsonManagedReference
	private Balance balance;
	

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn( name = "account_id")
	private List<Transaction> transactions=new ArrayList<Transaction>();
	
	public Account() {
	
	}


	public Account(long id, User user, Balance balance, List<Transaction> transactions) {

		this.id = id;
		this.user = user;
		this.balance = balance;
		this.transactions = transactions;
	}

	
	
	public List<Transaction> getTransaction() {
		return transactions;
	}

	 
	public void setTransaction(List<Transaction> transaction) {
		this.transactions = transaction;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}

	public Balance getBalance() {
		return balance;
	}

	public void setBalance(Balance balance) {
		this.balance = balance;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
