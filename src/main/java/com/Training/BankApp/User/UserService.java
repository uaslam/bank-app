package com.Training.BankApp.User;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;



@Service
public class UserService  implements UserDetailsService
{
	UserRepository userRepository;

	private final Logger logger = LoggerFactory.getLogger(getClass());

	public UserService(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;

	}

	public boolean create(User customer) {

		if (userRepository.findByEmail(customer.getEmail()) != null) {
			logger.warn("Customer with  email: " + customer.getEmail() + " already exists");
			return false;
		}

		return false;

	}

	public void delete(Long id) {
		userRepository.delete(id);
		
	}

	public User findById(Long id) {
		return userRepository.findOne(id);
	}

	@Override
	public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
		User user = userRepository.findByName(name);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid user");
		}
		return new org.springframework.security.core.userdetails.User(Long.toString(user.getId()), user.getPassword(),
				AuthorityUtils.commaSeparatedStringToAuthorityList(user.getRoles()));

	}

	public void update(User user) {
		userRepository.save(user);
		
	}
}
