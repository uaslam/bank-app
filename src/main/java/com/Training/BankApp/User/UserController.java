package com.Training.BankApp.User;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.Training.BankApp.Account.Account;

@RestController
public class UserController {

	UserService userService;
	private final Logger logger = LoggerFactory.getLogger(getClass());
	

	public UserController(UserService userService) {
		super();
		this.userService = userService;
	}

	@PostMapping("/api/user/add/{id}")
	public ResponseEntity<Void> UpdateAccount(@PathVariable("id") Long id, @RequestBody User user) {

		User existing = userService.findById(id);
		if (existing == null) {
			logger.warn("Customer with  ID: " + id + " does not exists for updation.");
			return ResponseEntity.badRequest().build();
		}

		user.setId(id);
		userService.update(user);
		logger.info("Customer with  ID: " + id + " successfully updated.");
		return ResponseEntity.ok().build();

	}
/*
	@PostMapping("/api/user/add")
	public ResponseEntity<Void> Create(@RequestBody User customer) {

		if (userService.create(customer)) {
			return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
					.buildAndExpand(customer.getId()).toUri()).build();
		}

		return ResponseEntity.badRequest().build();
	}
	*/
	@DeleteMapping("/api/user/delete/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
		User existing = userService.findById(id);
		if (existing != null) {
			userService.delete(id);
			logger.info("Customer with  ID: " + id + " successfully deleted.");
			return ResponseEntity.ok().build();
		}
		logger.warn("Customer with  ID: " + id + " does not exists for deletion");
		return ResponseEntity.badRequest().build();
	}
}
